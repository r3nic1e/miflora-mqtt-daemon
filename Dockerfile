ARG BASEIMAGE
FROM ${BASEIMAGE}

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app
COPY requirements.txt requirements.txt
RUN apt-get update && \
    apt-get install -y --no-install-recommends make gcc libglib2.0-dev && \
    pip install --no-cache-dir -r requirements.txt

COPY . .

ENTRYPOINT ["python3", "./miflora-mqtt-daemon.py"]